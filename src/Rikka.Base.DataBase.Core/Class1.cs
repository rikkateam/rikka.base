﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Rikka.Base.DataBase.Core
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTime CreatedOn { get; set; }
    }

    public class BaseEntityRepository<TContext, T> where T : class, IEntity
        where TContext: DbContext
    {
        protected readonly TContext Context;
        protected readonly DbSet<T> DbSet;

        public BaseEntityRepository(TContext context)
        {
            Context = context;
            DbSet = Context.Set<T>();
        }

        public async Task<T> GetById(Guid id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task Insert(T item)
        {
            if (item.Id == Guid.Empty)
                item.Id = Guid.NewGuid();
            item.CreatedOn = DateTime.Now;
            await DbSet.AddAsync(item);
            await Context.SaveChangesAsync();
        }

        public async Task Update(T item)
        {
            DbSet.Update(item);
            await Context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var item = await GetById(id);
            await Delete(item);
            await Context.SaveChangesAsync();
        }

        public async Task Delete(T item)
        {
            DbSet.Remove(item);
            await Context.SaveChangesAsync();
        }

    }
}
